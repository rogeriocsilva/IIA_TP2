﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AStarSearch : SearchAlgorithm {

    private HashSet<object> closedSet = new HashSet<object>();
    private List<SearchNode> lista;
    private SearchNode start;
    private bool goalNode;
    public int heuristic = 1;
    // Use this for initialization
    void Start()
    {
        goalNode = false;
        problem = GameObject.Find("Map").GetComponent<Map>().GetProblem();
        start = new SearchNode(problem.GetStartState(), 0);
        start.f = 0 + HeuristicRemainingGoals(start.state, problem);
        lista = new List<SearchNode>();
        lista.Add(start);
    }

    protected override void Step()
    {
        if (lista.Count == 0)
        {
            finished = true;
            running = false;
        }
        SearchNode cur_node = lista[0];
        lista.RemoveAt(0);
        if (problem.IsGoal(cur_node.state))
        {
            solution = cur_node;
            goalNode = true;
            finished = true;
            running = false;
        }
        else
        {
            Successor[] sucessors = problem.GetSuccessors(cur_node.state);
            foreach (Successor suc in sucessors)
            {
                if (!closedSet.Contains(suc.state))
                {
                    SearchNode new_node = null;
                    switch(heuristic){
                        case (1):
                            new_node = new SearchNode(suc.state, suc.cost + cur_node.g + HeuristicRemainingGoals(suc.state, problem), suc.action, cur_node);
                            break;
                        case (2):
                            new_node = new SearchNode(suc.state, suc.cost + cur_node.g + HeuristicBoxGoalDistance(suc.state, problem), suc.action, cur_node);
                            break;
                        case (3):
                            new_node = new SearchNode(suc.state, suc.cost + cur_node.g + HeuristicBoxGoalManhattan(suc.state, problem), suc.action, cur_node);
                            break;
                        case (4):
                            new_node = new SearchNode(suc.state, suc.cost + cur_node.g + HeuristicMinimumMatching(suc.state, problem), suc.action, cur_node);
                            break;
                        case (5):
                            new_node = new SearchNode(suc.state, suc.cost + cur_node.g + HeuristicRemainingGoalsDeadlock(suc.state, problem), suc.action, cur_node);
                            break;
                        case (6):
                            new_node = new SearchNode(suc.state, suc.cost + cur_node.g + HeuristicBoxGoalDistanceDeadlock(suc.state, problem), suc.action, cur_node);
                            break;
                        case (7):
                            new_node = new SearchNode(suc.state, suc.cost + cur_node.g + HeuristicBoxGoalManhattanDeadlock(suc.state, problem), suc.action, cur_node);
                            break;
                    }
                    int aux = lista.Count;
                    bool inserted = false;
                    for (int i = 0; i < aux; i++)
                    {
                        if (!inserted && lista[i].g > new_node.g)
                        {
                            lista.Insert(i, new_node);
                            inserted = true;
                        }
                    }
                    if (!inserted)
                    {
                        lista.Add(new_node);
                    }
                }
            }
        }


    }


}
