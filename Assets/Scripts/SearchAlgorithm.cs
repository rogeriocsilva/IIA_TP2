﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SearchNode
{	
	public object state;
	public float g;
	public float f;
	public Action action;
	public SearchNode parent;
	public int depth;

	public SearchNode(object state, float g, Action action=Action.None, SearchNode parent=null)
	{
		this.state = state;
		this.g = g;
		this.f = g;
		this.action = action;
		this.parent = parent;
		if (parent != null) {
			this.depth = parent.depth + 1;
		} else {
			this.depth = 0;
		}
	}

	public SearchNode(object state, float g, float h, Action action=Action.None, SearchNode parent=null)
	{
		this.state = state;
		this.g = g;
		this.f = g + h;
		this.action = action;
		this.parent = parent;
		if (parent != null) {
			this.depth = parent.depth + 1;
		} else {
			this.depth = 0;
		}
	}
}


public abstract class SearchAlgorithm : MonoBehaviour {

	public int stepsPerFrame = 10;
	[HideInInspector]public ISearchProblem problem;

	protected bool running = false;
	protected bool finished = false;
	protected SearchNode solution = null;

	void Update () {
		if (running && !finished) {
			for (int i = 0; i < stepsPerFrame; i++) {
				if (!finished) {
					Step ();
                    
				}
			}
		}
	}

	public bool Finished()
	{
		return finished;
	}

	public List<Action> GetActionPath()
	{
		if (finished && solution != null) {
			return BuildActionPath ();
		} else {
			Debug.LogWarning ("Solution path can not be determined! Either the algorithm has not finished, or a solution could not be found.");
			return null;
		}
	}

	// This method should be overriden on each specific search algorithm.
	protected abstract void Step ();

	public void StartRunning()
	{
		running = true;
	}

	private List<Action> BuildActionPath()
	{
		List<Action> path = new List<Action> ();
		SearchNode node = solution;

		while (node.parent != null) {
			path.Insert (0, node.action);
			node = node.parent;
		}

		return path;
	}


    public float HeuristicRemainingGoals(object state, object problem)
    {
        SokobanState s = (SokobanState)state;
        SokobanProblem p = (SokobanProblem)problem;
        int remainingGoals = p.goals.Count;
        foreach (Vector2 crate in s.crates)
        {
            if (p.goals.Contains(crate))
            {
                remainingGoals--;
            }
        }
        return remainingGoals;
    }

    public float HeuristicRemainingGoalsDeadlock(object state, object problem)
    {
        SokobanState s = (SokobanState)state;
        SokobanProblem p = (SokobanProblem)problem;
        int remainingGoals = p.goals.Count;
        int num_crates = s.crates.Count;
        int num_goals = p.goals.Count;
        for (int i = 0; i < num_crates; i++)
        {
            if (p.goals.Contains(s.crates[i]))
            {
                remainingGoals--;
            }
            else
            {
                bool dead = true;
                for (int j = 0; j < num_goals; j++)
                {
                    if (!CheckSimpleDeadlocks(s, p, (int)s.crates[i].x, (int)s.crates[i].y, (int)p.goals[j].x, (int)p.goals[j].y))
                    {
                        dead = false;
                    }
                }
                if (dead)
                {
                    return 10000000;
                }
            }



        }
        return remainingGoals;
    }

    public float HeuristicBoxGoalDistance(object state, object problem)
    {
        SokobanState s = (SokobanState)state;
        SokobanProblem p = (SokobanProblem)problem;
        float max = 0, min;
        float distancia = float.MaxValue;

        foreach (Vector2 crate in s.crates)
        {
            min = float.MaxValue;
            if (!p.goals.Contains(crate))
            {
                foreach (Vector2 goal in p.goals)
                {
                    if (Vector2.Distance(crate, goal) < min)
                    {
                        min = Vector2.Distance(crate, goal) / 4;
                    }


                }

                if (distancia > Vector2.Distance(crate, new Vector2(s.player.x, s.player.y)) / 4)
                {
                    distancia = Vector2.Distance(crate, new Vector2(s.player.x, s.player.y)) / 4;
                }
            }
            else
            {
                continue;
            }
            max += min;
        }

        /*Adicionar a distancia player - crate*/
        if (distancia != float.MaxValue)
        {
            max += distancia;
        }
        return max;
    }

    public float HeuristicBoxGoalDistanceDeadlock(object state, object problem)
    {
        SokobanState s = (SokobanState)state;
        SokobanProblem p = (SokobanProblem)problem;
        float max = 0, min;
        float distancia = float.MaxValue;

        foreach (Vector2 crate in s.crates)
        {
            min = float.MaxValue;
            if (!p.goals.Contains(crate))
            {
                bool dead = true;

                foreach (Vector2 goal in p.goals)
                {
                    if (Vector2.Distance(crate, goal) < min)
                    {
                        min = Vector2.Distance(crate, goal) / 4;
                    }
                    if (!CheckSimpleDeadlocks(s, p, (int)crate.x, (int)crate.y, (int)goal.x, (int)goal.y))
                    {
                        dead = false;
                    }

                }

                if (dead){
                    return 10000000;
                }

                if (distancia > Vector2.Distance(crate, new Vector2(s.player.x, s.player.y)) / 4){
                    distancia = Vector2.Distance(crate, new Vector2(s.player.x, s.player.y)) / 4;
                }
            }
            else
            {
                continue;
            }
            max += min;
        }

        /*Adicionar a distancia player - crate*/
        if (distancia != float.MaxValue)
        {
            max += distancia;
        }
        return max;
    }




    public float HeuristicBoxGoalManhattan(object state, object problem)
    {
        SokobanState s = (SokobanState)state;
        SokobanProblem p = (SokobanProblem)problem;
        float max = 0, min;
        float distancia = float.MaxValue;

        foreach (Vector2 crate in s.crates)
        {
            min = float.MaxValue;
            if (!p.goals.Contains(crate))
            {
                foreach (Vector2 goal in p.goals)
                {
                    if ((Mathf.Abs(crate.x - goal.x) + Mathf.Abs(crate.y - goal.y)) < min)
                    {
                        min = (Mathf.Abs(crate.x - goal.x) + Mathf.Abs(crate.y - goal.y));
                    }


                }

                if (distancia > ((Mathf.Abs(crate.x - s.player.x) + Mathf.Abs(crate.y - s.player.y))))
                {
                    distancia = (Mathf.Abs(crate.x - s.player.x) + Mathf.Abs(crate.y - s.player.y));
                }
            }
            else
            {
                continue;
            }
            max += min;
        }

        /*Adicionar a distancia player - crate*/
        if (distancia != float.MaxValue)
        {
            max += distancia;
        }
        return max;
    }


    public float HeuristicBoxGoalManhattanDeadlock(object state, object problem)
    {
        SokobanState s = (SokobanState)state;
        SokobanProblem p = (SokobanProblem)problem;
        float max = 0, min;
        float distancia = float.MaxValue;

        foreach (Vector2 crate in s.crates)
        {
            min = float.MaxValue;
            if (!p.goals.Contains(crate))
            {
                bool dead = true;
                foreach (Vector2 goal in p.goals)
                {
                    if ((Mathf.Abs(crate.x - goal.x) + Mathf.Abs(crate.y - goal.y)) < min)
                    {
                        min = (Mathf.Abs(crate.x - goal.x) + Mathf.Abs(crate.y - goal.y));
                    }
                    if (!CheckSimpleDeadlocks(s, p, (int)crate.x, (int)crate.y, (int)goal.x, (int)goal.y))
                    {
                        dead = false;
                    }

                }

                if (dead)
                {
                    return 10000000;
                }

                if (distancia > ((Mathf.Abs(crate.x - s.player.x) + Mathf.Abs(crate.y - s.player.y))))
                {
                    distancia = (Mathf.Abs(crate.x - s.player.x) + Mathf.Abs(crate.y - s.player.y));
                }
            }
            else
            {
                continue;
            }
            max += min;
        }

        /*Adicionar a distancia player - crate*/
        if (distancia != float.MaxValue)
        {
            max += distancia;
        }
        return max;
    }

    int [,] tabela;
    int n;
    int m;
    int best = int.MaxValue;
    int [] current_table;

    public float HeuristicMinimumMatching(object state, object problem)
    {
        best = int.MaxValue;
        SokobanState s = (SokobanState)state;
        SokobanProblem p = (SokobanProblem)problem;
        int num_crates = s.crates.Count;
        int num_goals = p.goals.Count;
        n = num_crates;
        m = num_goals;
        tabela = new int[n,m];
        current_table = new int[m];
        for(int k = 0; k < m; k++)
        {
            current_table[k] = 0;
        }

        /*preencher a tabela criada*/
        for(int i = 0; i < num_crates; i++)
        {
            for (int j = 0; j < num_goals; j++)
            {
                /* distancia de manhattan entre goal e crate*/
                if (CheckSimpleDeadlocks(s, p, (int)s.crates[i].x, (int)s.crates[i].y, (int)p.goals[j].x, (int)p.goals[j].y))
                {
                    //print("deadlock");
                    tabela[i,j] = 10000000;
                }
                else
                {
                    tabela[i,j] = (int)((Mathf.Abs(s.crates[i].x - p.goals[j].x) + Mathf.Abs(s.crates[i].y - p.goals[j].y)));
                }
            }
        }
        LpaWeek3Adapted();
        return best;
    }


    public void LpaWeek3Adapted()
    {
        int i;
        for (i = 0; i < m; i++)
        {
            current_table[i] += tabela[i,0];
            if (n == 1)
            {
                if (current_table[i] < best)
                {
                    best = current_table[i];
                }
            }
            else
            {
                doStuff(0, current_table[i]);
            }
            current_table[i] -= tabela[i,0];
        }
    }


    public void doStuff(int y, int total)
    {       /*y: crate 				total: maior tempo do array*/
        int i, totalaux;

        if (total >= best){           /*Condicao de backtracking 1*/
            return;
        }
        if (y == n - 1){           /*Caso base*/
            best = total;       /*atualizar o melhor*/
            return;
        }
        for (i = 0; i < m; i++)
        {
            /*caso ainda n tenha sido atribuido uma crate ao goal*/
            if (current_table[i] == 0)
            {
                totalaux = total + tabela[i , y + 1];
                if (totalaux < best)
                {
                    current_table[i] += tabela[i, y + 1];     /*Somar o valor do caminho*/
                    doStuff(y + 1, tempo());
                    current_table[i] -= tabela[i, y + 1];     /*voltar a tras*/
                }
            }
        }
    }

    int tempo()
    {       /*retorna a soma de current*/
        int i;
        int max = 0;
        for (i = 0; i < m; i++)
        {
                max += current_table[i];
        }
        return max;
    }


    public bool CheckSimpleDeadlocks(object state, object problem, int crate_x, int crate_y, int goal_x, int goal_y)
    {
        SokobanState s = (SokobanState)state;
        SokobanProblem p = (SokobanProblem)problem;
        bool dead = false;
        bool ingoal = p.goals.Contains(new Vector2(crate_x,crate_y));
        /* caso canto*/
        if (!ingoal)
        {
            /* caso cantos*/
            if (p.walls[crate_y, crate_x + 1] || p.walls[crate_y, crate_x - 1])
            {
                if (p.walls[crate_y + 1, crate_x] || p.walls[crate_y - 1, crate_x])
                {
                    dead = true;
                    return dead;
                }
            }
            /*caso vertical deadlock to goal - lado direito*/
            if (p.walls[crate_y, crate_x + 1])
            {
                bool top_closed=false, bottom_closed=false, searching=true;
                int i = 1;
                while (searching && (p.walls[crate_y+i, crate_x + 1] || p.walls[crate_y+i, crate_x]))
                {
                    if (crate_x == goal_x && crate_y == goal_y + i)
                    {
                        return false;
                    }
                    if (p.walls[crate_y + i, crate_x])
                    {
                        top_closed = true;
                        searching = false;
                    }
                    i++;
                }
                searching = true;
                i = -1;
                while (searching && (p.walls[crate_y + i, crate_x + 1] || p.walls[crate_y + i, crate_x]))
                {
                    if (crate_x == goal_x && crate_y == goal_y + i)
                    {
                        return false;
                    }
                    if (p.walls[crate_y + i, crate_x])
                    {
                        bottom_closed = true;
                        searching = false;
                    }
                    i--;
                }
                if(top_closed && bottom_closed)
                {
                    return true;
                }

            }
            /*caso vertical deadlock to goal - lado esquerdp*/
            if (p.walls[ crate_y, crate_x - 1])
            {
                bool top_closed = false, bottom_closed = false, searching = true;
                int i = 1;
                while (searching && (p.walls[ crate_y + i, crate_x - 1] || p.walls[ crate_y + i, crate_x]))
                {
                    if (crate_x == goal_x && crate_y + i == goal_y)
                    {
                        return false;
                    }
                    if (p.walls[crate_y + i, crate_x])
                    {
                        top_closed = true;
                        searching = false;
                    }
                    i++;
                }
                searching = true;
                i = -1;
                while (searching && (p.walls[ crate_y + i, crate_x - 1] || p.walls[ crate_y + i, crate_x]))
                {
                    if (crate_x == goal_x && crate_y + i == goal_y)
                    {
                        return false;
                    }
                    if (p.walls[crate_y + i, crate_x])
                    {
                        bottom_closed = true;
                        searching = false;
                    }
                    i--;
                }
                if (top_closed && bottom_closed)
                {
                    return true;
                }
            }
            /*caso horizontal deadlock - por cima*/
            if (p.walls[crate_y+1, crate_x])
            {
                bool right_closed = false, left_closed = false, searching = true;
                int i = 1;
                while (searching && (p.walls[crate_y + 1, crate_x + i] || p.walls[crate_y, crate_x + i]))
                {
                    if (crate_x + i == goal_x && crate_y == goal_y)
                    {
                        return false;
                    }
                    if (p.walls[crate_y, crate_x + i])
                    {
                        right_closed = true;
                        searching = false;
                    }
                    i++;
                }
                searching = true;
                i = -1;
                while (searching && (p.walls[crate_y + 1, crate_x + i] || p.walls[crate_y, crate_x + i]))
                {
                    if (crate_x +i == goal_x && crate_y == goal_y)
                    {
                        return false;
                    }
                    if (p.walls[crate_y, crate_x + i])
                    {
                        left_closed = true;
                        searching = false;
                    }
                    i--;
                }
                if (right_closed && left_closed)
                {
                    return true;
                }
            }
            /*caso horizontal deadlock - por baixo*/
            if (p.walls[crate_y - 1, crate_x])
            {
                bool right_closed = false, left_closed = false, searching = true;
                int i = 1;
                while (searching && (p.walls[crate_y - 1, crate_x + i] || p.walls[crate_y, crate_x + i]))
                {
                    if (crate_x + i == goal_x && crate_y == goal_y)
                    {
                        return false;
                    }
                    if (p.walls[crate_y, crate_x + i])
                    {
                        right_closed = true;
                        searching = false;
                    }
                    i++;
                }
                searching = true;
                i = -1;
                while (searching && (p.walls[crate_y - 1, crate_x + i] || p.walls[crate_y, crate_x + i]))
                {
                    if (crate_x + i == goal_x && crate_y == goal_y)
                    {
                        return false;
                    }
                    if (p.walls[ crate_y, crate_x + i])
                    {
                        left_closed = true;
                        searching = false;
                    }
                    i--;
                }
                if (right_closed && left_closed)
                {
                    return true;
                }
            }
        }

        return dead;
    }
}


