using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProgressiveDepthSearch : SearchAlgorithm {
	
	private HashSet<object> closedSet = new HashSet<object> ();
	private Stack<SearchNode> openQueue;
	private SearchNode start;
	private bool goalNode; 

	void Start () 
	{
		goalNode = false;
		problem = GameObject.Find ("Map").GetComponent<Map> ().GetProblem();
		start = new SearchNode (problem.GetStartState (), 0);
		openQueue = new Stack<SearchNode> ();
		openQueue.Push (start);
	}

	protected override void Step(){
		int i = 0;
		while(!goalNode){
			DLS(i);
			i++;
		}
		
	}

	protected void DLS(int limit){
        openQueue.Clear();
        closedSet.Clear();
        start = new SearchNode(problem.GetStartState(), 0);
        openQueue.Push(start);
        while (openQueue.Count > 0){
			SearchNode cur_node = openQueue.Pop();
			closedSet.Add (cur_node.state);

			if (problem.IsGoal (cur_node.state) && cur_node.depth <=limit) {
				solution = cur_node;
				goalNode = true;
				finished = true;
				running = false;
			} 

			else if(cur_node.depth < limit){
				Successor[] sucessors = problem.GetSuccessors (cur_node.state);
				foreach (Successor suc in sucessors) {
					if (!closedSet.Contains (suc.state) ) {
						SearchNode new_node = new SearchNode (suc.state, suc.cost + cur_node.g, suc.action, cur_node);
						openQueue.Push (new_node);
					}
				}
			}
		}
		
		finished = true;
		running = false;
		
	}

}